// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-messaging-compat.js');


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB5syg_DEI8Ns8xtXQE_5GglBfjiqED0RA",
  authDomain: "pushnotificationtest-90d92.firebaseapp.com",
  projectId: "pushnotificationtest-90d92",
  storageBucket: "pushnotificationtest-90d92.appspot.com",
  messagingSenderId: "865986786173",
  appId: "1:865986786173:web:5039e6945071f2eeafd214"
};

console.log('initializing firebase')
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.onBackgroundMessage((payload) => {
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: payload.notification.image,
  };
  self.registration.showNotification(notificationTitle, notificationOptions);
});
// messaging.onBackgroundMessage((payload) => {
//   console.log('[firebase-messaging-sw.js] Received background message ', payload);
//   const notificationTitle = payload.notification.title;
//   const notificationOptions = { body: payload.notification.body, };
//   return self.registration.showNotification(notificationTitle, notificationOptions);
// });
// console.log(messaging)
// self.addEventListener('notificationclick', event => { console.log(event) });
// Retrieve firebase messaging
// console.log('messaging', messaging)
// messaging.getToken({ vapidKey: "BH4-PLg_Zq14c1yIe8xsBXuqryklbvs-0R98EwkqH64jSFlq4irbjZmc0Vyxu-mFYF9WaZFI2wCfnvAj2MN2rzA" }).then((fcmToken) => {
//   console.log(fcmToken)
//   messaging.onMessage((payload) => { console.log("onMessage event fired", payload) })
// });
//https://stackoverflow.com/questions/56791717/firebase-messaging-gettoken-not-working-as-expected-on-google-chrome
