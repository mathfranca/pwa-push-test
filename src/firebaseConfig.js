import { initializeApp } from "firebase/app";

import { getMessaging } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyB5syg_DEI8Ns8xtXQE_5GglBfjiqED0RA",
  authDomain: "pushnotificationtest-90d92.firebaseapp.com",
  projectId: "pushnotificationtest-90d92",
  storageBucket: "pushnotificationtest-90d92.appspot.com",
  messagingSenderId: "865986786173",
  appId: "1:865986786173:web:5039e6945071f2eeafd214"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Messaging service
export const messaging = getMessaging(app)

